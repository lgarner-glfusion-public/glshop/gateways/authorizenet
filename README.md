# Authorize.Net Payment Gateway

This is the payment gateway module for Authorize.Net, for the glFusion shop plugin.

Only cart checkout is supported by this gateway.

## Authorize.Net Account

## Gateway Configuration
Configure the gateway in the Shop plugin administration area.

### Sandbox Location ID
Enter the ID of a sandbox location. There should be a few created automatically for you when you created your app.

### Sandbox App ID
Enter the Application ID the corresponds to the sandbox location.

### Sandbox Access Token
Enter the Personal Access Token that corresponds to the Sandbox App ID.

### Production Location ID
Enter the ID of your production location.

### Production App ID
Enter the Application ID the corresponds to the production location.

### Production Access Token
Enter the Personal Access Token that corresponds to the Production App ID.

### Testing (Sandbox) Mode?
Check this to use the sandbox to test your processes. Once you are ready to go live, uncheck this box.

### Enabled
Check to enable this gateway. If it is not enabled, no buy-now buttons will be shown for it and it cannot be used during checkout.

### Order
This indicates the order in which the gateway will appear on the checkout form, lower numbers appear first.
