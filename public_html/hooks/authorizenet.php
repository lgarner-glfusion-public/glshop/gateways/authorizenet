<?php
/**
 * Webhook endpoint for Authorize.Net.
 * Authorize.Net does not support $_GET arguments in the webhook.
 *
 * @author      Lee Garner <lee@leegarner.com>
 * @copyright   Copyright (c) 2020-2022 Lee Garner <lee@leegarner.com>
 * @package     shop
 * @version     v1.4.1
 * @since       v1.3.0
 * @license     http://opensource.org/licenses/gpl-2.0.php
 *              GNU Public License v2 or later
 * @filesource
 */

/** Import core glFusion functions */
require_once '../../lib-common.php';
use Shop\Log;

$gw_name = 'authorizenet';
if (empty($gw_name)) {
    Log::warn("Gateway not specified in Webhook message data");
} else {
    Log::debug("Received $gw_name Webhook:");
}

// Log everything before instantiating the webhook handler in case
// something goes wrong later.
Log::debug("Got Webhook Headers: " . var_export($_SERVER,true));
Log::debug("Got Webhook GET: " . var_export($_GET, true));
Log::debug("Got Webhook POST: " . var_export($_POST, true));
Log::debug("Got php:://input: " . var_export(@file_get_contents('php://input'), true));

// Get the complete IPN message prior to any processing
if (!empty($gw_name)) {
    $WH = Shop\Webhook::getInstance($gw_name);
    if ($WH && $WH->Verify()) {
        $WH->Dispatch();
    } else {
        Log::error("Webhook verification failed for $gw_name");
    }
}
echo "Completed.";
exit;
